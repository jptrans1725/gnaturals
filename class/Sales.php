<?php
require_once('../database/Database.php');
require_once('../interface/iSales.php');
class Sales extends Database implements iSales {
	public function new_sales($code,$generic,$brand,$gram,$type,$qty,$price,$customer_name,$customer_contact,$customer_address,$cart_uniqid,$customer_id,$admin_id)
	{
		$sql = "INSERT INTO sales(item_code,generic_name,brand,gram,type,qty,price,customer_name,customer_contact,customer_address,cart_uniqid,customer_id,admin_id)
				VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
		return $this->insertRow($sql, [$code,$generic,$brand,$gram,$type,$qty,$price,$customer_name,$customer_contact,$customer_address,$cart_uniqid,$customer_id,$admin_id]);
	}//end new_sales

	public function daily_sales($date)
	{
		$sql = "SELECT *
				FROM sales
				WHERE DATE(`date_sold`) = ?";
		return $this->getRows($sql, [$date]);
	}//end daily_sales

	public function getSales($fromDateTime, $toDateTime)
	{
		$sql = "SELECT *
				FROM sales
				WHERE date_sold >= ?
				AND date_sold <= ?";
		return $this->getRows($sql, [$fromDateTime, $toDateTime]);
	}

	public function getSalesPerUniqid($uniqid)
	{
		$sql = "SELECT *
				FROM sales
				WHERE cart_uniqid = ?";
		return $this->getRows($sql, [$uniqid]);
	}

}//end class
$sales = new Sales();

/* End of file Sales.php */
/* Location: .//D/xampp/htdocs/regis/class/Sales.php */