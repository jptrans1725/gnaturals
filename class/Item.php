<?php
require_once('../database/Database.php');
require_once('../interface/iItem.php');
class Item extends Database implements iItem {
	public function all_items($where = null)
	{
		$sql = "SELECT *
				FROM item i 
				INNER JOIN item_type it 
				ON i.item_type_id = it.item_type_id
				ORDER BY i.item_name ASC";

		if ($where !== null && $where !== '') {
			$myfile = fopen("../debug.txt", "w") or die("Unable to open file!");
			
			fwrite($myfile, $where);
			fclose($myfile);

			$sql = "SELECT *
				FROM item i 
				INNER JOIN item_type it 
				ON i.item_type_id = it.item_type_id
				WHERE i.item_code = '$where'
				ORDER BY i.item_name ASC";
		}

		return $this->getRows($sql);
	}//end all_items
	
	public function get_item($item_id)
	{
		$sql = "SELECT *
				FROM item
				WHERE item_id = ?";
		return $this->getRow($sql, [$item_id]);
	}//end edit_item

	public function add_item($iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile, $min_stock)
	{
		$sql = "INSERT INTO item(item_name, item_price, item_type_id, item_code, item_brand, item_grams, item_qr, item_min)
				VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
		return $this->insertRow($sql, [$iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile, $min_stock]);
	}//end add_item

	public function edit_item($item_id, $iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile,$item_min)
	{
		if ($qrCodeFile !== null && $qrCodeFile !== '') {

			$sql = "UPDATE item 
					SET item_name = ?, item_price = ?, item_type_id = ?, item_code = ?, item_brand = ?, item_grams = ?, item_qr = ?, item_min = ?
					WHERE item_id = ?";
			return $this->updateRow($sql, [$iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile, $item_min, $item_id]);
		} else {
			$sql = "UPDATE item 
					SET item_name = ?, item_price = ?, item_type_id = ?, item_code = ?, item_brand = ?, item_grams = ?, item_min = ?
					WHERE item_id = ?";
			return $this->updateRow($sql, [$iName, $iPrice, $type_id, $code, $brand, $grams, $item_min, $item_id]);

		}
	}//end edit_item
}//end class Item

$item = new Item();

/* End of file Item.php */
/* Location: .//D/xampp/htdocs/regis/class/Item.php */