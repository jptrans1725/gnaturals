<?php
class Delivery extends Database {

	public function getOrders($courier_id)
	{
		$sql = "SELECT *, SUM((c.qty* i.item_price)) AS 'total_price' FROM sales c
				LEFT JOIN item i ON c.item_code = i.item_code
				/*WHERE c.courier_id = ?*/
				GROUP BY c.cart_uniqid";
		return $this->getRows($sql, [$courier_id]);
	}

	public function getOrder($cart_uniq_id)
	{
		$sql = "SELECT *, (c.qty* i.item_price) as 'item_total', SUM((c.qty* i.item_price)) AS 'total_price' FROM sales c
				LEFT JOIN item i ON c.item_code = i.item_code
				WHERE c.cart_uniqid = ?
				GROUP BY c.sales_id";
		return $this->getRows($sql, [$cart_uniq_id]);
	}

	public function updateOrderStatus($cart_uniq_id, $status, $lat, $long, $courier_id)
	{
		if ($lat == null || $long == null) {
			return false;
		}
		$sql = "UPDATE sales SET status=?, lat_=?, long_=?, date_delivered=NOW(), courier_id = ?
				WHERE cart_uniqid=?";
		return $this->updateRow($sql, [$status, $lat, $long, $courier_id, $cart_uniq_id]);
	}
}//end class
