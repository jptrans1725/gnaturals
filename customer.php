<?php 
    require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php
    $system = new System_Setup();
    $system_details = $system->getSystemSetup();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$system_details['system_name']?> Inventory & Monitoring System</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="assets/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php"><?=$system_details['system_name']?> Inventory and Monitoring System</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>  <?= $_SESSION['user_account'] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href=""><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="customer.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></i> Home</a>
                    </li>
                    <li>
                        <a href="delivery.php"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a>
                    </li>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome <small><?= ucwords($_SESSION['user_fullname']) ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-home"></i> Home
                            </li>
                        </ol>
                    </div>
                </div>
                <!--Start QR-->
                <div class="row" style="margin-bottom: 20px;display: none;">
                    <input type="hidden" name="qrReadPage" id="qrReadPage" value="home">
                    <button class="btn btn-default" id="search-qr">Search QR
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                    <input type="hidden" name="qrRead" id="qrRead" value="0">
                    <div id="qr-output"></div>
                </div>

                <!--End QR-->
                    <!-- /.row -->
                    <div class="row"> 
                        <div id="order" style="margin-top: 10% !important;"></div>               
                    </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


<?php include_once('modal/to_cart.php'); ?>
<?php include_once('modal/confirmation.php'); ?>
<?php include_once('modal/add_new_item.php'); ?>
<?php include_once('modal/message.php'); ?>

    <script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/regis.js"></script>
    <div id="qr-footer"></div>
    <script type="text/javascript">
    $("#search-qr").click(function(){
        var value = $("#qrRead").val();

        if (value == 0) {
            $.ajax({
               url: 'modal/qr.php',
               success: function(html) {
                  $("#qr-output").append(html);
                  $.ajax({
                    url:'modal/qr_footer.php',
                    success: function(data){
                        $("#qr-footer").append(data);
                        $("#qrRead").val('1');
                    }
                  });
               }
            });
        } else {
            $("#qr-output").html('');
            $("#qr-footer").html('');
            $("#qrRead").val('0');
            window.location.reload();
        }
    
    });
</script>

</body>

</html>
