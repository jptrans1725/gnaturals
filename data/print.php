<?php 
require_once('../class/Stock.php');
require_once('../class/System_Setup.php');
require_once('../include/session.php');

$system = new System_Setup();
$system_details = $system->getSystemSetup();

$stock = new Stock();

$fromDateTime = getDataGET("fromDateTime");
$toDateTime = getDataGET("toDateTime");

$stocks = $stock->all_stockListFromTo($fromDateTime, $toDateTime);

?>
<?php
    function getDataGET($name){
        if (isset($_GET[$name])) {
            return $_GET[$name];
        }
        return "";
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$system_details['system_name']?> Inventory and Delivery Monitoring System</title>

    <!-- Bootstrap Core CSS -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-theme.min.css">
    <link href="../assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript">
        print();
    </script>
</head>
<body>


<center>
    <h2><?=$system_details['system_name']?> Inventory and Delivery Monitoring System</h2>
    <h3>as of</h3>
    <h3>From : <?php echo date_format(date_create($fromDateTime),"m-d-Y H:i:s"); ?></h3>
    <h3>To : <?php echo date_format(date_create($toDateTime),"m-d-Y H:i:s"); ?></h3>
    <h3>Prepared by : <?=$_SESSION['user_fullname']?></h3>
</center>

<br />
<div class="table-responsive">
        <table id="myTable-stock" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th><center>Item Name</center></th>
                    <th><center>Price</center></th>
                    <th><center>Quantity</center></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($stocks as $s): ?>
                <tr align="left">
                    <td><?= ucwords($s['item_name']); ?></td>
                    <td><?= "Php ".number_format($s['item_price'], 2); ?></td>
                    <td><?= $s['stock_qty']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
</div>


<?php 
$stock->Disconnect();
$system->Disconnect();
 ?>



    <script type="text/javascript">
        print();
    </script>
</body>
</html>
