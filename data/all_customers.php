<?php

include_once '../database/custom_db.php';

$qry = 'SELECT * FROM user where user_type = 3 order by user_fullname';
$result = $conn->query($qry);
?>
<div class="table-responsive">
        <table id="myTable-customers" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th><center>Fullname</center></th>
                    <th><center>Contact</center></th>
                    <th><center>Address</center></th>
                    <th width="120px"><center>Action</center></th>
                </tr>
            </thead>
            <tbody>
            <?php while ($customer = $result->fetch_array()){ ?>
                <tr align="left">
                    <td><?= ucwords($customer['user_fullname']) ?></td>
                    <td><?= $customer['user_contact'] ?></td>
                    <td><?= $customer['user_address'] ?></td>
                    <td>
                        <button onclick="editCustomerModal('<?= $customer['user_id']; ?>');" type="button" class="btn btn-warning btn-xs">Edit
                         <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </button>
                        <button onclick="delCustomer('<?= $customer['user_id']; ?>');" type="button" class="btn btn-warning btn-xs">Remove
                         <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-customers').DataTable();
    });
</script>
