<?php 
require_once('../class/Cart.php');
require_once('../class/Sales.php');
if(isset($_POST['click'])){
	if($_POST['click'] == 'yes'){
		//select cart details
		$cartDetails = $cart->allCart($_SESSION['logged_id']);
		$customer_name = $_POST['customer_name'];
		$customer_contact = $_POST['customer_contact'];
		$customer_address = $_POST['customer_address'];
		$customer_id = ($_SESSION['logged_type'] != 1) ? $_SESSION['logged_id'] : 0;
		$admin_id = ($_SESSION['logged_type'] == 1) ? $_SESSION['logged_id'] : null;
		foreach ($cartDetails as $cd) {
		    $code = $cd['item_code'];
            $generic = $cd['item_name'];
            $brand = $cd['item_brand'];
            $gram = $cd['item_grams'];
            $type = $cd['item_type_desc'];
            $cartQty = $cd['cart_qty'];
            $price = $cd['item_price'];
            $cart_uniqid = $cd['cart_uniqid'];
			
			$insertSale = $sales->new_sales($code,$generic,$brand,$gram,$type,$cartQty,$price,$customer_name,$customer_contact,$customer_address,$cart_uniqid,$customer_id,$admin_id);
			//insert to sales
		}//end foreach

		//del all cart
		$delAllCart = $cart->dellAllCart($cart_uniqid);
		$return['valid'] = false;
		if($delAllCart){
			$return['valid'] = true;
			$return['msg'] = 'Transaction is added successfully!';
		}
		echo json_encode($return);
	}//end yes
}//end isset