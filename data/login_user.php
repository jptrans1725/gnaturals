<?php 
require_once('../class/User.php');

if(isset($_POST['un'])){
	$un = $_POST['un'];
	$up = $_POST['up'];

	$up = md5($up);
	$result = $user->user_login($un, $up);
	if($result > 0){
		// echo 'succ';
		$return['logged'] = true;
		$return['url'] = "home.php";
		$_SESSION['logged_id'] = $result['user_id'];
		$_SESSION['logged_type'] = $result['user_type'];
		$_SESSION['user_account'] = $result['user_account'];
		$_SESSION['user_fullname'] = $result['user_fullname'];
		$_SESSION['user_contact'] = $result['user_contact'];
		$_SESSION['user_address'] = $result['user_address'];
		//$_SESSION['uniqid'] = uniqid();

		/* CHECK IF COURIER USER */
		if($result['user_type'] == "1"){
			$_SESSION['user_type'] = "Administrator";
		}else if($result['user_type'] == "2"){
			$_SESSION['user_type'] = "Courier";
			$return['url'] = "delivery.php";
		}else{
			$return['user_type'] = "Customer";
			$return['url'] = "customer.php";
		}
	}else{
		// echo 'fail';
		$return['logged'] = false;
		$return['msg'] = "Invalid Username or Password!";
	}

	echo json_encode($return);

}//end isset

$user->Disconnect();