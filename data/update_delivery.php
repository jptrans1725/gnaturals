<?php 
require_once('../database/Database.php');
require_once('../class/Delivery.php');
$delivery = new Delivery();

if(isset($_POST['cart_uniq_id'])){
	$cart_uniq_id = $_POST['cart_uniq_id'];
	$status = $_POST['status'];
	$lat = $_POST['lat'];
	$long = $_POST['long'];
	$courier_id = $_SESSION['logged_id'];

	$delivery_update_status = $delivery->updateOrderStatus($cart_uniq_id, $status, $lat, $long, $courier_id);

	print_r($delivery_update_status);
}//end isset
