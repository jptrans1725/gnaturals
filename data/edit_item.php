<?php 
require_once('../class/Item.php');
if(isset($_POST['item-id'])){
	$item_id = $_POST['item-id'];
	$iName = $_POST['item-name'];
	$iPrice = $_POST['item-price'];
	$iType = $_POST['item-type'];
	$code = $_POST['code'];
	$brand = $_POST['brand'];
	$grams = $_POST['grams'];
	$item_min = $_POST['min_stock'];
	$qrCodeFile = null;

	$type = explode('.', $_FILES['qrCodeFile']['name']);
	$type = $type[count($type)-1];		
	$url = '../qr_image/'.$_FILES['qrCodeFile']['name'];

	if(in_array($type, array('gif', 'jpg', 'jpeg', 'png', 'JPG', 'GIF', 'JPEG', 'PNG'))) {
		if(is_uploaded_file($_FILES['qrCodeFile']['tmp_name'])) {			
			if(move_uploaded_file($_FILES['qrCodeFile']['tmp_name'], $url)) {
				$qrCodeFile = $_FILES['qrCodeFile']['name'];
			}
		}
	}

	$saveEdit = $item->edit_item($item_id, $iName, $iPrice, $iType, $code, $brand, $grams, $qrCodeFile, $item_min);
	$return['valid'] = false;
	if($saveEdit){
		$return['valid'] = true;
		$return['msg'] = "Edit Successfully!";
	}
	echo json_encode($return);
}//end isset
$item->Disconnect();
