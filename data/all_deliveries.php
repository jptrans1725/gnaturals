<?php 
    require_once('../database/Database.php');
    require_once('../class/Delivery.php');
    
    $delivery = new Delivery();
    $courier_id = $_SESSION['logged_id'];
    $deliveries = $delivery->getOrders($courier_id);

    $delivery->Disconnect();
?>

<br />
<form class="table-responsive" method="post" action="delivery-item.php" id="form_">
    <div style="margin-bottom: 2% !important;">

        <button type="button" class="btn btn-warning" id="btnToggleScan" onclick="toggleVideo()">Scan</button>

        <button type="submit" class="btn btn-success" name="btnItem_" id="btnItem_" form="form_" style="display: none !important">View Order Details</button>

        <div class="btn-group btn-group-toggle mb-5" data-toggle="buttons" id="divCameraSetup" style="display: none !important">
            <label class="btn btn-primary active">
                <input type="radio" name="options" value="1" autocomplete="off" checked> Front Camera
            </label>
            <label class="btn btn-secondary">
                <input type="radio" name="options" value="2" autocomplete="off"> Back Camera
            </label>
            <label class="btn btn-secondary">

            </label>
        </div>

        <video id="preview" hidden></video>



    </div>

    <table id="myTable-item" class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>Sales Unique ID</th>
                <th>Date Ordered</th>
                <th>Total Price</th>
                <th>Status</th>
                <th>
                    <center>Action</center>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($deliveries as $delivery_item): ?>
                <?php
                    if ($_SESSION['logged_id'] != $delivery_item['customer_id'] && $_SESSION['logged_type'] == 3) {
                        continue;
                    }
                ?>
                <tr align="center">
                    <td align="left"><?= $delivery_item['cart_uniqid']; ?></td>
                    <td align="left"><?= $delivery_item['date_sold']; ?></td>
                    <td><?= "Php ".number_format($delivery_item['total_price'], 2); ?></td>
                    <td align="left"><?= $delivery_item['status']; ?></td>
                    <td>
                       <center>
                           <button name="btnItem_<?= $delivery_item['cart_uniqid']; ?>" type="submit" class="btn btn-warning btn-xs">View
                            <span class="glyphicon glyphicon-eye" aria-hidden="true"></span>
                            </button>
                       </center>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</form>


<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-item').DataTable({
            "order" : [[3, 'desc'],[1, 'desc']]
        });
    });
</script>




<script type="text/javascript">
    var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });

    scanner.addListener('scan',function(content){
        // window.location.href=content;

        alert("Sales Unique ID : " + content);
        
        document.getElementById("btnItem_").name = "btnItem_" + content.trim();
        document.getElementById("btnItem_").click();
        console.log("btnItem_" + content.trim());


    });

    function toggleVideo(){
        if(document.getElementById('preview').hidden){
            document.getElementById('preview').hidden = false;
            document.getElementById('btnItem_').hidden = false;
            document.getElementById("btnItem_").style.display = "block";
            document.getElementById("divCameraSetup").style.display = "block";
            document.getElementById('btnToggleScan').innerText = "Cancel";
            
            Instascan.Camera.getCameras().then(function (cameras){
                if(cameras.length>0){
                    if(cameras.length>1){
                        scanner.start(cameras[1]);
                    }else{
                        scanner.start(cameras[0]);                
                    }
                    $('[name="options"]').on('change',function(){
                        if($(this).val()==1){
                            if(cameras[0]!=""){
                                scanner.start(cameras[0]);
                            }else{
                                alert('No Front camera found!');
                            }
                        }else if($(this).val()==2){
                            if(cameras[1]!=""){
                                scanner.start(cameras[1]);
                            }else{
                                alert('No Back camera found!');
                            }
                        }
                    });
                }else{
                    console.error('No cameras found.');
                    alert('No cameras found.');
                }
            }).catch(function(e){
                console.error(e);
                alert(e);
            });            
        }else{
            document.getElementById('preview').hidden = true;
            document.getElementById('btnItem_').hidden = true;
            document.getElementById("btnItem_").style.display = "none";
            document.getElementById("divCameraSetup").style.display = "none";
            document.getElementById('btnToggleScan').innerText = "Scan";
            scanner.stop();
        }
    }




</script>
