<?php 
require_once('../class/Sales.php');
require_once('../class/System_Setup.php');
require_once('../include/session.php');

$system = new System_Setup();
$system_details = $system->getSystemSetup();

$uniqid = getDataGET("uniqid");

//$sales_ = $sales->getSales($fromDateTime, $toDateTime);
$sales_ = $sales->getSalesPerUniqid($uniqid);

?>
<?php
    function getDataGET($name){
        if (isset($_GET[$name])) {
            return $_GET[$name];
        }
        return "";
    }
?>
 <!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/bootstrap-theme.min.css">
    <!-- Font Awesome -->
    <link href="../assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="../assets/qrcodeCreate/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/qrcodeCreate/qrcode.js"></script>


    <script type="text/javascript">
        //print();
    </script>
  </head>
  <body>
    
 <center>
    <h1><?=$system_details['system_name']?> Order Details</h1>
    <h3>Order Uniqid : <?= $uniqid ?></h3>
    <div id="qrcode"></div>
    <h3>Prepared by : <?=$_SESSION['user_fullname']?></h3>
 </center>
 <h4>Customer Name : <?= $sales_[0]['customer_name'] ?></h4>
 <h4>Customer Contact : <?= $sales_[0]['customer_contact'] ?></h4>
 <h4>Customer Address : <?= $sales_[0]['customer_address'] ?></h4>
<br />
<div class="table-responsive">
        <table id="myTable-sales" class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <th>Item Code</th>
                    <th>Item Name</th>
                    <th>Brand</th>
                    <th><center>Grams</center></th>
                    <th><center>Type</center></th>
                    <th><center>Price</center></th>
                    <th><center>Qty</center></th>
                    <th><center>Sub Total</center></th>
                </tr>
            </thead>
            <tbody>
            <?php 
                $total = 0;
                foreach($sales_ as $ds):
                $subTotal =($ds['price'] * $ds['qty']);   
                $total += $subTotal; 
            ?>
                <tr>
                    <td><?= $ds['item_code']; ?></td>
                    <td><?= $ds['generic_name']; ?></td>
                    <td><?= $ds['brand']; ?></td>
                    <td><?= $ds['gram']; ?></td>
                    <td><?= $ds['type']; ?></td>
                    <td align="center"><?= number_format($ds['price'],2 ); ?></td>
                    <td align="center"><?= $ds['qty']; ?></td>
                    <td align="center"><?= $subTotal; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td align="right"><strong>TOTAL:</strong></td>
                <td align="center">
                    <strong><?= number_format($total,2); ?></strong>
                </td>
            </tr>
        </table>
</div>
<input type="hidden" name="text" id="text" value="<?= $uniqid ?>">

<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

<!-- for the datatable of employee -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable-sales').DataTable();
    });


var qrcode = new QRCode(document.getElementById("qrcode"), {
    width : 100,
    height : 100
});

function makeCode () {      
    var elText = document.getElementById("text");
    
    if (!elText.value) {
        alert("Input a text");
        elText.focus();
        return;
    }
    
    qrcode.makeCode(elText.value);
}

makeCode();

$("#text").
    on("blur", function () {
        makeCode();
    }).
    on("keydown", function (e) {
        if (e.keyCode == 13) {
            makeCode();
        }
    });
    print();
</script>

<?php 
$sales->Disconnect();
$system->Disconnect();
 ?>