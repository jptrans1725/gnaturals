<?php 
require_once('../class/Item.php');
if(isset($_POST['item-name']) && isset($_POST['item-price'])){
	$iName = $_POST['item-name'];
	$iPrice = $_POST['item-price'];
	$iType = $_POST['item-type'];
	$code = $_POST['code'];
	$brand = $_POST['brand'];
	$grams = $_POST['grams'];
	$qrCodeFile = null;
	$min_stock = $_POST['min_stock'];

	$type = explode('.', $_FILES['qrCodeFile']['name']);
	$type = $type[count($type)-1];		
	$url = '../qr_image/'.$_FILES['qrCodeFile']['name'];

	if(in_array($type, array('gif', 'jpg', 'jpeg', 'png', 'JPG', 'GIF', 'JPEG', 'PNG'))) {
		if(is_uploaded_file($_FILES['qrCodeFile']['tmp_name'])) {			
			if(move_uploaded_file($_FILES['qrCodeFile']['tmp_name'], $url)) {
				$qrCodeFile = $_FILES['qrCodeFile']['name'];
			}
		}
	}

	$iName = strtolower($iName);
	$iPrice = strtolower($iPrice);
	$iName = ucwords(strtolower($iName));
	$code = ucwords(strtolower($code));
	$brand = ucwords(strtolower($brand));
	$grams = ucwords(strtolower($grams));

	$saveItem = $item->add_item($iName, $iPrice, $iType, $code, $brand, $grams, $qrCodeFile, $min_stock);
	if($saveItem){
		$return['valid'] = true;
		$return['msg'] = "New Record Added Successfully!";
	}else{
		$return['valid'] = false;
	}
	echo json_encode($return);
}//end isset

$item->Disconnect();