<?php

include_once '../database/custom_db.php';

if (isset($_POST['id'])) {
	$id = $_POST['id'];
	$data = array();
	$qry = "SELECT * FROM user where user_id = '$id'";
	if ($result = $conn->query($qry)) {
		$data['status'] = true;
		$row = $result->fetch_array();
		$data['user_id'] = $row['user_id'];
		$data['user_account'] = $row['user_account'];
		$data['user_fullname'] = $row['user_fullname'];
		$data['user_contact'] = $row['user_contact'];
		$data['user_address'] = $row['user_address'];

		echo json_encode($data); 
	} else {
		return false;
	}
} else {
	return false;	
}

?>