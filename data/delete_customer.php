<?php

include_once '../database/custom_db.php';

$data = ['status' => false, 'message' => 'Failed on removing.'];

if (isset($_POST['id'])) {

	$qry = "DELETE FROM user WHERE user_id = " . $_POST['id'];
	
	if ($result = $conn->query($qry)) {
		$data = ['status' => true, 'message' => 'Successfully removed.'];
	}

}

echo json_encode($data);
?>