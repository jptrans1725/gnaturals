<?php 
interface iItem{
	public function all_items($where = null);
	public function get_item($item_id);
	public function add_item($iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile, $min_stock);
	public function edit_item($item_id, $iName, $iPrice, $type_id, $code, $brand, $grams, $qrCodeFile,$item_min);
}//end iItem