-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 07, 2021 at 05:39 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `regis`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `cart_qty` int(11) NOT NULL,
  `cart_stock_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_uniqid` varchar(35) NOT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `item_id` (`item_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expired`
--

DROP TABLE IF EXISTS `expired`;
CREATE TABLE IF NOT EXISTS `expired` (
  `exp_id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_itemName` varchar(50) NOT NULL,
  `exp_itemPrice` float NOT NULL,
  `exp_itemQty` int(11) NOT NULL,
  `exp_expiredDate` date NOT NULL,
  PRIMARY KEY (`exp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(50) NOT NULL,
  `item_price` double NOT NULL,
  `item_type_id` int(11) NOT NULL,
  `item_code` varchar(35) NOT NULL,
  `item_brand` varchar(50) NOT NULL,
  `item_grams` varchar(20) NOT NULL,
  `item_qr` varchar(255) DEFAULT NULL,
  `item_min` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_code` (`item_code`),
  KEY `item_type_id` (`item_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `item_name`, `item_price`, `item_type_id`, `item_code`, `item_brand`, `item_grams`, `item_qr`, `item_min`) VALUES
(26, '100% Organic Virgin Coconut Oil', 250, 1, 'OVCO', 'Gnaturals', '250ml', NULL, 1),
(27, '3-in-1 VCO Germinal Shampoo & Conditioner', 250, 2, 'GSC', 'Gnaturals', '200ml', NULL, 1),
(28, 'Delicate Touch Feminine Wash 300ml', 250, 2, 'FW300', 'Gnaturals', '300ml', NULL, 1),
(29, 'Skincare Hand Sanitizer ', 299, 2, 'HS30', 'Gnaturals', '300ml', NULL, 1),
(30, 'Skincare Liquid Soap', 249, 2, 'LS300', 'Gnaturals', '300ml', NULL, 1),
(31, 'Skincare Insect Repellent Citronella', 299, 1, 'IRC250', 'Gnaturals', '250ml', NULL, 1),
(32, 'Skincare Insect Repellent Lemongrass', 299, 1, 'IRL250', 'Gnaturals', '250ml', NULL, 1),
(33, 'Travel Pack With Citronella Insect Repellent', 350, 1, 'TPC', 'Gnaturals', '300ml', NULL, 1),
(34, 'Travel Pack With Lemongrass Insect Repellent', 350, 1, 'TPL', 'Gnaturals', '300ml', NULL, 1),
(35, 'Skincare Soap VCO Pure & Mild', 139, 2, 'VSP', 'Gnaturals', '125g', NULL, 1),
(36, 'Skincare Soap VCO with Carabao Milk', 159, 2, 'VSCM', 'Gnaturals', '125g', NULL, 1),
(37, 'Skincare Soap VCO with Coffee Scrub', 139, 2, 'VSCS', 'Gnaturals', '125g', NULL, 1),
(38, 'Skincare Soap VCO With Glutamansi', 139, 2, 'VSG', 'Gnaturals', '125g', NULL, 1),
(39, 'Skincare Soap VCO with Collagen', 159, 2, 'VSC', 'Gnaturals', '125g', NULL, 1),
(40, 'Body Massage Oil Eucalyptus Breath-in & Relax ', 170, 1, 'MOE', 'Gnaturals', '120ml', NULL, 1),
(41, 'Body Massage Oil Vanilla Sweet Sensation', 170, 1, 'MOV', 'Gnaturals', '120ml', NULL, 1),
(42, 'Body Massage Oil Lavander Healing & Calmness', 170, 1, 'MOL', 'Gnaturals', '120ml', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

DROP TABLE IF EXISTS `item_type`;
CREATE TABLE IF NOT EXISTS `item_type` (
  `item_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type_desc` varchar(50) NOT NULL,
  PRIMARY KEY (`item_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`item_type_id`, `item_type_desc`) VALUES
(1, 'Oil'),
(2, 'Soap'),
(3, 'Sanitizer');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
CREATE TABLE IF NOT EXISTS `sales` (
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_code` varchar(35) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `brand` varchar(35) NOT NULL,
  `gram` varchar(35) NOT NULL,
  `type` varchar(35) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` float NOT NULL,
  `date_sold` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cart_uniqid` varchar(35) NOT NULL,
  `courier_id` int(11) DEFAULT NULL,
  `date_delivered` timestamp NULL DEFAULT NULL,
  `status` varchar(50) DEFAULT 'For delivery',
  `lat_` varchar(50) DEFAULT NULL,
  `long_` varchar(50) DEFAULT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_contact` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sales_id`, `item_code`, `generic_name`, `brand`, `gram`, `type`, `qty`, `price`, `date_sold`, `cart_uniqid`, `courier_id`, `date_delivered`, `status`, `lat_`, `long_`, `customer_name`, `customer_address`, `customer_contact`, `customer_id`, `admin_id`) VALUES
(47, 'FW300', 'Delicate Touch Feminine Wash 300ml', 'Gnaturals', '300ml', 'Soap', 2, 250, '2021-05-06 01:43:59', '609349d0c4d1c', NULL, NULL, 'For delivery', NULL, NULL, 'Testing', 'Valenzuela', '911', 0, NULL),
(48, 'FW300', 'Delicate Touch Feminine Wash 300ml', 'Gnaturals', '300ml', 'Soap', 50, 250, '2021-05-07 01:42:03', '609494b87c16e', NULL, NULL, 'For delivery', NULL, NULL, 'A', 'c', 'b', 0, NULL),
(49, 'FW300', 'Delicate Touch Feminine Wash 300ml', 'Gnaturals', '300ml', 'Soap', 10, 250, '2021-05-07 01:45:55', '60949bc31d866', NULL, NULL, 'For delivery', NULL, NULL, 'k', 'k', 'k', 0, NULL),
(50, 'FW300', 'Delicate Touch Feminine Wash 300ml', 'Gnaturals', '300ml', 'Soap', 30, 250, '2021-05-07 01:54:12', '60949bce58ee0', 3, '2021-05-07 02:29:52', 'Delivered', '14.567353666666666', '121.10806533333331', 'p', 'p', 'p', 0, NULL),
(51, 'FW300', 'Delicate Touch Feminine Wash 300ml', 'Gnaturals', '300ml', 'Soap', 100, 250, '2021-05-07 02:06:43', '60949db9d333b', NULL, NULL, 'For delivery', NULL, NULL, 'jpt', 'abc', '09123456789', 17, NULL),
(52, 'GSC', '3-in-1 VCO Germinal Shampoo & Conditioner', 'Gnaturals', '200ml', 'Soap', 50, 250, '2021-05-07 02:07:13', '6094a0c9cb7bd', NULL, NULL, 'For delivery', NULL, NULL, 'i', 'i', 'i', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `stock_qty` int(11) NOT NULL,
  `stock_expiry` date NOT NULL,
  `stock_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stock_manufactured` date NOT NULL,
  `stock_purchased` date NOT NULL,
  `stock_supplier` varchar(255) NOT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `item_id`, `stock_qty`, `stock_expiry`, `stock_added`, `stock_manufactured`, `stock_purchased`, `stock_supplier`) VALUES
(20, 26, 980, '2021-08-19', '2021-05-05 09:45:12', '2021-05-01', '2021-05-02', 'Gnaturals'),
(21, 27, 948, '2021-11-30', '2021-05-07 02:07:05', '2021-05-01', '2021-05-02', 'Gnaturals'),
(22, 40, 996, '2021-11-30', '2021-05-05 12:41:03', '2021-05-01', '2021-05-02', 'Gnaturals'),
(23, 42, 837, '2021-11-30', '2021-05-05 12:16:11', '2021-05-01', '2021-05-02', 'Gnaturals'),
(24, 41, 1000, '2021-11-30', '2021-05-05 10:21:53', '2021-05-01', '2021-05-02', 'Gnaturals'),
(25, 28, 800, '2021-11-30', '2021-05-07 01:54:01', '2021-05-01', '2021-05-02', 'Gnaturals'),
(26, 29, 979, '2021-11-30', '2021-05-05 22:21:47', '2021-05-01', '2021-05-02', 'Gnaturals'),
(27, 31, 1000, '2021-11-30', '2021-05-05 10:25:08', '2021-05-01', '2021-05-02', 'Gnaturals'),
(29, 32, 1000, '2021-09-29', '2021-05-05 10:25:43', '2021-05-01', '2021-05-02', 'Gnaturals'),
(30, 30, 999, '2021-09-29', '2021-05-05 23:23:54', '2021-05-01', '2021-05-02', 'Gnaturals'),
(31, 35, 1000, '2021-11-24', '2021-05-05 10:26:59', '2021-05-01', '2021-05-02', 'Gnaturals'),
(32, 36, 1000, '2021-11-24', '2021-05-05 10:27:03', '2021-05-01', '2021-05-02', 'Gnaturals'),
(33, 37, 1000, '2021-11-24', '2021-05-05 10:27:07', '2021-05-01', '2021-05-02', 'Gnaturals'),
(34, 39, 1000, '2021-11-24', '2021-05-05 10:27:13', '2021-05-01', '2021-05-02', 'Gnaturals'),
(35, 38, 1000, '2021-11-24', '2021-05-05 10:27:17', '2021-05-01', '2021-05-02', 'Gnaturals'),
(36, 33, 1000, '2021-11-24', '2021-05-05 10:27:21', '2021-05-01', '2021-05-02', 'Gnaturals'),
(37, 34, 1000, '2021-11-24', '2021-05-05 10:27:26', '2021-05-01', '2021-05-02', 'Gnaturals');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(50) NOT NULL,
  `user_pass` varchar(35) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_contact` varchar(255) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_account` (`user_account`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_account`, `user_pass`, `user_type`, `user_fullname`, `user_contact`, `user_address`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '', '', ''),
(2, 'admin2', '21232f297a57a5a743894a0e4a801fc3', 1, '', '', ''),
(3, 'courier', '21232f297a57a5a743894a0e4a801fc3', 2, '', '', ''),
(4, '1', '5', 3, '2', '3', '48800'),
(5, '7', '7', 3, '7', '7', '7'),
(6, '3', '9', 3, '32', '5', '6'),
(7, '5', '6', 3, '8', '9', '5'),
(9, '10', '6', 3, '8', '9', '5'),
(10, 'po', 'p', 3, 'p', 'p', 'p'),
(12, 'jpt', '$pswx', 3, '1234', '46176899', '13215678557'),
(14, 'opi', 'po', 3, 'opiopi', 'opi', 'poipo'),
(15, '4564564', '4564', 3, '5645', '6456', '45689'),
(16, '13123', 'c7e1249ffc03eb9ded908c236bd1996d', 3, '789', '7897', '9879'),
(17, 'jptrans', '3553b3c0ec7c2515b373d2ab2d9e2efa', 3, 'jpt', '09123456789', 'abc'),
(18, '97897987', '908075ea2c025c335f4865f7db427062', 3, '98789', '7897', '89798');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`item_type_id`) REFERENCES `item_type` (`item_type_id`);

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
