<?php 
require_once('database/Database.php');
$db = new Database();
$sql = "SELECT *
		FROM item_type
		ORDER BY item_type_desc ASC";
$types = $db->getRows($sql);
// echo '<pre>';
// 	print_r($types);
// echo '</pre>';
 ?>
<div class="modal fade" id="modal-item">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="row">
					<div class="col-md-11">
						<h4 class="modal-title">Modal title</h4>
					</div>
					<div class="col-md-1">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
				</div>
			</div>
			<div class="modal-body">
			
				<form class="form-horizontal" role="form" id="form-item" enctype="multipart/form-data">
				<input type="hidden" id="item-id" name="item-id">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Item Name:</label>
				    <div class="col-sm-9">
				      <input type="text" maxlength="50" class="form-control" name="item-name" id="item-name" placeholder="Enter Item Name" required="" autofocus="">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Price:</label>
				    <div class="col-sm-9"> 
				      <input type="number" min="0.1" step="any" class="form-control" name="item-price" id="item-price" placeholder="Enter Price" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Item Code:</label>
				    <div class="col-sm-9">
				      <input type="text" maxlength="50" class="form-control" name="code" id="code" placeholder="Enter Item Code" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Brand Name:</label>
				    <div class="col-sm-9">
				      <input type="text" maxlength="50" class="form-control" name="brand" id="brand" placeholder="Enter Brand Name" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Volume:</label>
				    <div class="col-sm-9">
				      <input type="text" maxlength="50" class="form-control" name="grams" id="grams" placeholder="Enter Volume(ml./g)" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Min. Stock:</label>
				    <div class="col-sm-9">
				      <input type="number" maxlength="50" class="form-control" name="min_stock" id="min_stock" placeholder="Enter Minimum Stock">
				    </div>
				  </div>

				  <div class="form-group">
				  	<label class="control-label col-sm-3" for="">Type:</label>
				   <div class="col-sm-4"> 				    
				      <select id="item-type" name="item-type" class="btn btn-default form-control">
				      	<?php foreach($types as $t): ?>
				      		<option value="<?= $t['item_type_id']; ?>"><?= ucwords($t['item_type_desc']); ?></option>
				      	<?php endforeach; ?>
				      </select>
				    </div>
				   <div class="col-sm-5">
				   		<input type="file" name="qrCodeFile" id="qrCodeFile" class="form-control">
				    </div>
				  </div>
				  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" id="submit-item" value="add" class="btn btn-default">Save
				      <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
				      </button>
				      <button type="button" id="downloadQr" class="btn btn-info">Download QR
				      <span class="glyphicon glyphicon-download" aria-hidden="true"></span>
				      </button>
				    </div>
				  <div class="form-group">
					<div class="col-md-12">
					  <center>
						<div id="test"></div>
						QR CODE HERE
						<div id="qrcode" style="width:150px; height: 150px; margin-top:15px;"></div>
					</center>
					</div>
				  </div>
				  </div>
				</form>
				
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<?php 
$db->Disconnect();
 ?>