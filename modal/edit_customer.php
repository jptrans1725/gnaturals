<div class="modal fade" id="modal-customer">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="row">
					<div class="col-md-11">
						<h4 class="modal-title">Edit Customer Profile</h4>
					</div>
					<div class="col-md-1">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
				</div>
			</div>
			<div class="modal-body">
			
				<form class="form-horizontal" role="form" id="form-customer">
					<input type="hidden" name="user_id" id="user_id">

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Username:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" name="user_account" id="user_account" required="" readonly>
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Name:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" name="user_fullname" id="user_fullname" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Contact:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" name="user_contact" id="user_contact" required="">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="">Address:</label>
				    <div class="col-sm-9">
				      <input type="text" class="form-control" name="user_address" id="user_address" required="">
				    </div>
				  </div>

				  <div class="form-group"> 
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" id="submit-item" value="add" class="btn btn-default">Save
				      <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
				      </button>
				    </div>
				  </div>
				</form>
				
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>