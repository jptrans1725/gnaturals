<div class="modal fade" id="modal-confirmation">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title text-info">Provide customer details to confirm.</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						  <div class="form-group">
						    <label class="control-label col-md-3" for="">Customer Name:</label>
						    <div class="col-md-9">
						      <input type="text" maxlength="50" class="form-control" name="customer-name" id="customer-name" placeholder="Enter Customer Name" required autofocus
						      <?= ($_SESSION['logged_type'] == 3) ? 'readonly' : '' ?>
						      value="<?= ($_SESSION['logged_type'] == 3) ? $_SESSION['user_fullname'] : '' ?>">
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-3" for="">Contact Number:</label>
						    <div class="col-md-9">
						      <input type="text" maxlength="50" class="form-control" name="contact-number" id="contact-number" placeholder="Enter Contact Number" required 
						      <?= ($_SESSION['logged_type'] == 3) ? 'readonly' : '' ?>
						      value="<?= ($_SESSION['logged_type'] == 3) ? $_SESSION['user_contact'] : '' ?>">
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-md-3" for="">Delivery Address:</label>
						    <div class="col-md-9">
						      <input type="text" maxlength="50" class="form-control" name="delivery-address" id="delivery-address" placeholder="Enter Delivery Address" required
						      <?= ($_SESSION['logged_type'] == 3) ? 'readonly' : '' ?>
						      value="<?= ($_SESSION['logged_type'] == 3) ? $_SESSION['user_address'] : '' ?>">
						    </div>
						  </div>
						  <div class="form-group">
						  	<div class="col-md-3"></div>
						  	<div class="col-md-3">
								<button id="confirm-yes" type="button" class="btn btn-default btn-lg del-expired" >Confirm
									<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								</button>
							</div>
						  	<div class="col-md-3">
								<input type="hidden" id="confirm-type" value="null">
								<button type="button" class="btn btn-default btn-lg" data-dismiss="modal" >Declined
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</div>
						  	<div class="col-md-3"></div>	  	
							</div>
						  </div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>