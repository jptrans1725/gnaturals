<?php 
    //require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php
    $system = new System_Setup();
    $system_details = $system->getSystemSetup();
?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title><?=$system_details['system_name']?> Inventory Monitoring System</title>
        <link rel="stylesheet" href="assets/css/style.css">
  </head>

  <body style="background: #262626;">
  <br/>
  <br/>    
<div class="container col-md-4">

  <div id="login-form">
    <fieldset>
    <h3>Customer Registration</h3>

      <form id="form-register">
        <div class="form-group">
          <input type="text" autofocus name="user_account" id="user_account" placeholder="Username" required autocomplete="off" class="form-control"> 
        </div>
        <div class="form-group">
          <input type="text" autofocus id="user_fullname" name="user_fullname" placeholder="Fullname" required autocomplete="off" class="form-control"> 
        </div>
        <div class="form-group">
          <input type="text" autofocus id="user_contact" name="user_contact" placeholder="Phone Number" required autocomplete="off" class="form-control"> 
        </div>
        <div class="form-group">
          <input type="text" autofocus id="user_address" name="user_address" placeholder="Address" required autocomplete="off" class="form-control"> 
        </div>
        <div class="form-group">
          <input type="password" id="password" name="password" placeholder="Password" required autocomplete="off">
        </div>
        <div class="form-group">
          <div class="row" style="margin-top:20px">
            <input type="submit" name="submit" value="Register">
            <a href='index.php'> Click here to Login. </a>
          </div>
        </div>
        
      </form>
</fieldset>

  </div> <!-- end login-form -->

</div>
    
    
    
<script type="text/javascript" src="assets/js/jquery-1.12.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/regis.js"></script>
<script>
$(document).ready(function(){
    $("#form-register").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var formData = new FormData(this);
        $.ajax({
            url: 'data/customer_registration.php',
            type: 'post',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success:function(data) {
                    console.log(data);
                    if (data.status == true) {
                      var stat = confirm(data.message + ' Would you like to login now?');
                      if (stat == true) {
                        window.location.href='index.php';
                      }
                    }
            },
            error: function(xhr, status, error) {
              var err = eval(xhr.responseText);
              alert(err);
            },
        });
    });
});
</script>
    
  </body>
</html>



 