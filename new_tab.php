<?php 
    require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php
    $system = new System_Setup();
    $system_details = $system->getSystemSetup();
?>
<?php 
  $db = new Database();
  //get all items
  $sql = "SELECT *
      FROM item 
      ORDER BY item_name ASC";
  $items = $db->getRows($sql);
 ?>
<title>QR CODE RESULT</title>

<head> 

</head>
<body>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <form class="form-horizontal" role="form" id="form-stock">
          <form action="database/Database.php" method="GET">
          <div class="form-group">
            <label class="control-label col-sm-3" for="">Item:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="item-id" value="100% Organic Virgin Coconut Oil" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3" for="">Quantity:</label>
            <div class="col-sm-9"> 
              <input type="number" min="1" class="form-control" id="qty" placeholder="Enter Quantity" required="">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="">Expiry Date:</label>
            <div class="col-sm-9"> 
              <input type="date" value="2023-12-25" class="form-control" id="xDate" disabled="">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="">Manufactured:</label>
            <div class="col-sm-9"> 
              <input type="date" value="2021-01-01" class="form-control" id="manu" disabled="">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-3" for="">Purchased:</label>
            <div class="col-sm-9"> 
              <input type="date" value="2020-01-03" class="form-control" id="purc" disabled="">
            </div>
          </div>


          <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
              <button type="submit" class="btn btn-primary">Save
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </button>
            </div>
          </div>

        </form>
