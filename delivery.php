<?php 
    require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php
    $system = new System_Setup();
    $system_details = $system->getSystemSetup();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$system_details['system_name']?> Inventory & Monitoring System</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="assets/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
        #preview{
           width:500px;
           height: 500px;
           margin:0px auto;
        }

    </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php"><?=$system_details['system_name']?> Inventory and Monitoring System</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                        <?=$_SESSION['user_account']?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <?php
                        if($_SESSION['logged_type'] == '1'){
                    ?>
                            <li>
                                <a href="home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></i> Home</a>
                            </li>
                            <li>
                                <a href="item.php"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Item List</a>
                            </li>
                            <li>
                                <a href="product.php"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Product Profile</a>
                            </li>
                            <li>
                                <a href="stock.php"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Stocks</a>
                            </li>
                            <li>
                                <a href="expired.php"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Expired</a>
                            </li>
                            <li>
                                <a href="sales.php"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> Sales</a>
                            </li>
                            <li>
                                <a href="customer_management.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Customer Management</a>
                            </li>
                    <?php
                        }
                    ?>
                    <?php if ($_SESSION['logged_type'] == 3) : ?>
                        <li  class="active">
                            <!-- <a href="http://en.aika168.com/Monitor.aspx"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a> -->

                            <a href="customer.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                Home
                            </a>
                        </li>
                    <?php endif; ?>

                    <li  class="active">
                        <!-- <a href="http://en.aika168.com/Monitor.aspx"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a> -->
                        <a href="delivery.php"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid" style="padding-top: 20% !important;">
                
                <div id="all-deliveries"></div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.12.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/regis.js"></script>

<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

</body>

</html>


<script type="text/javascript">
    /*  COURIER FUNCTIONS  */

    //all item
    function showAllDeliveries()
    {
        $.ajax({
                url: 'data/all_deliveries.php',
                success: function (data) {
                    $('#all-deliveries').html(data);
                },
                error: function(){
                    alert('Error: L42+');
                }
            });
    }//end showAllItem


    showAllDeliveries();
</script>

