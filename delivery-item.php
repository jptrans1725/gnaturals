<?php 
    require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php
    $system = new System_Setup();
    $system_details = $system->getSystemSetup();

    $cart_unique_id = "";
    foreach ($_POST as $key => $value) {
        if(strpos($key, 'btnItem') !== false){
            $cart_unique_id = explode("_", $key)[1];
        }
    }

    $return = array(
            "cart_uniq_id" => "",
            "date_created" => "",
            "date_updated" => "",
            "status" => "",
            "gps_location" => "",
            "customer_name" => "",
            "customer_contact" => "",
            "customer_address" => "",
            "total_price" => 0,
            "items" => []
    );

    $orderDetails = [];

    if(!empty($cart_unique_id)){
        require_once('database/Database.php');
        require_once('class/Delivery.php');
        $delivery = new Delivery();

        $orderDetails = $delivery->getOrder($cart_unique_id);

        foreach($orderDetails as $delivery_item){
            if ($_SESSION['logged_id'] != $delivery_item['customer_id'] && $_SESSION['logged_type'] == 3) {
                continue;
            }
            $return['cart_uniq_id'] = $delivery_item['cart_uniqid'];
            $return['date_created'] = !empty($delivery_item['date_sold']) && $delivery_item['date_sold'] != "0000-00-00 00:00:00"? date('M d, Y | H:i:s', strtotime($delivery_item['date_sold'])) : "--";
            
            $return['status'] = $delivery_item['status'];
            $return['total_price'] += $delivery_item['total_price'];

            $return['customer_name'] = $delivery_item['customer_name'];
            $return['customer_contact'] = $delivery_item['customer_address'];
            $return['customer_address'] = $delivery_item['customer_contact'];

            

            $return['items'] = $orderDetails;
        
            if($delivery_item['status'] == "Delivered"){
                $return['date_updated'] = !empty($delivery_item['date_delivered']) && $delivery_item['date_delivered'] != "0000-00-00 00:00:00"? date('M d, Y | H:i:s', strtotime($delivery_item['date_delivered'])) : "--";

                if(!empty($delivery_item['lat_'])){
                    $return['gps_location'] = "https://www.google.com/maps/place/" . $delivery_item['lat_'] . "," . $delivery_item['long_'];
                    $return['gps'] = $delivery_item['lat_'] . "/" . $delivery_item['long_'];
                }
            }
        }

        $delivery->Disconnect();
        
    }//end isset



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?=$system_details['system_name']?> Inventory & Monitoring System</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="assets/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="assets/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet">


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php"><?=$system_details['system_name']?> Inventory and Monitoring System</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?=$_SESSION['user_type']?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <?php
                        if($_SESSION['logged_type'] == '1'){
                    ?>
                            <li>
                                <a href="home.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></i> Home</a>
                            </li>
                            <li>
                                <a href="item.php"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Item List</a>
                            </li>
                            <li>
                                <a href="product.php"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Product Profile</a>
                            </li>
                            <li>
                                <a href="stock.php"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Stocks</a>
                            </li>
                            <li>
                                <a href="expired.php"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Expired</a>
                            </li>
                            <li>
                                <a href="sales.php"><span class="glyphicon glyphicon-record" aria-hidden="true"></span> Sales</a>
                            </li>
                    <?php
                        }
                    ?>
                    

                    <li  class="active">
                        <!-- <a href="http://en.aika168.com/Monitor.aspx"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a> -->
                        <a href="delivery.php"><span class="glyphicon glyphicon-pushpin" aria-hidden="true"></span> Delivery Tracking</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid" style="padding-top: 20% !important;">

                <div class="container">
                    <h1>Sales Details</h1>
                </div>
                
                <div style="display:<?=empty($return['cart_uniq_id'])?"block":"none"?> !important;">

                   <div class="container" style="margin-bottom: 1% !important;">
                        <h3>
                            Order Unique ID is empty
                        </h3>
                    </div>  

                </div>

                <div style="display:<?=empty($orderDetails)?"block":"none"?> !important;">
                    
                    <div class="container" style="margin-bottom: 1% !important;">
                        <h3>
                            No records found for <?=$return['cart_uniq_id']?>
                        </h3>
                    </div> 

                </div>

                <div id="all-deliveries" style="display:<?=!empty($orderDetails)?"block":"none"?> !important;">

                    <div class="container" style="margin-bottom: 1% !important;">
                        <div class="container row m-0 p-0 justify-content-right text-right col-sm-10">
                            <?php if ($_SESSION['logged_type'] == 1) : ?>                        
                            <button class="btn btn-success btn-sm" id="sales-receipt">PRINT
                                <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            </button>
                            <?php endif ?>
                            <a href="delivery.php">
                                <button class="col-3 btn btn-info">
                                    Back
                                </button>
                            </a>

                            <button style="display: <?=$return['status'] == "Delivered" || $_SESSION['logged_type'] != "2"?"none":""?> !important;" class="col-4 btn btn-success" id="submit-item" <?=$return['status'] == "Delivered"?"disabled":""?>>
                                Update to Delivered
                            </button>
                        </div>
                    </div>
                    
                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Cart Items:</label>

                        <div class="col-sm-9 row m-0 p-0"> 
                            <div class="col-sm-3">Name</div>
                            <div class="col-sm-2">Price</div>
                            <div class="col-sm-2">Quantity</div>
                            <div class="col-sm-3">Item Total Price</div>
                        </div>
                    </div>

                    <?php
                        foreach ($return['items'] as $key => $item) {
                    ?>

                        <div class="container" style="margin-bottom: 1% !important;">
                            <label class="control-label col-sm-3" for=""></label>

                            <div class="col-sm-9 row m-0 p-0"> 
                                <div class="col-sm-3"><?=$item['item_name']?></div>
                                <div class="col-sm-2"><?=number_format($item['item_price'],2)?></div>
                                <div class="col-sm-2"><?=number_format($item['qty'],2)?></div>
                                <div class="col-sm-3"><?=number_format(($item['item_price']*$item['qty']),2)?></div>
                            </div>
                        </div>

                    <?php
                        }
                    ?>


                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Total Price:</label>
                        <div class="col-sm-7"> 
                          <input type="text" class="form-control" readonly value="<?=number_format($return['total_price'],2)?>">
                        </div>
                    </div>


                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Sales Unique ID:</label>
                        <div class="col-sm-7">
                            <input type="text"class="form-control" id="uniqid" readonly value="<?=$return['cart_uniq_id']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Date Ordered:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['date_created']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Customer Name:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['customer_name']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Customer Contact Number:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['customer_address']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Customer Address:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['customer_contact']?>">
                        </div>
                    </div>

                    
                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Status:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['status']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Date Delivered:</label>
                        <div class="col-sm-7"> 
                            <input type="text" class="form-control" readonly value="<?=$return['date_updated']?>">
                        </div>
                    </div>

                    <div class="container" style="margin-bottom: 1% !important;">
                        <label class="control-label col-sm-3" for="">Delivered to this location:</label>
                        <div class="col-sm-7"> 
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="" href="<?=$return['gps_location']?>" target="_blank" <?= empty($return['gps_location'])?"disabled":""?>>
                                        <button class="btn btn-warning" <?= empty($return['gps_location'])?"disabled":""?>>
                                            <?= empty($return['gps_location'])?"Location is not available":"Click here to see on maps"?>         
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-8">
                                    <label>GPS:<span><?=(!empty($return['gps'])) ? $return['gps'] : null?></span></label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


<?php include_once('modal/message.php'); ?>

<script type="text/javascript" src="assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.12.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/regis.js"></script>



</body>

</html>


<script type="text/javascript">
    var lat = "";
    var long = "";

    
    getCurrentLocation();

    function getCurrentLocation(){
        navigator.geolocation.getCurrentPosition(function(position) {
          console.log('Geolocation permissions granted');
          console.log('Latitude:' + position.coords.latitude);
          console.log('Longitude:' + position.coords.longitude);
          lat = position.coords.latitude;
          long = position.coords.longitude;
        });
    }


    function updateOrderToReceived(cart_uniq_id, status){
        $.ajax({
            url: 'data/update_delivery.php',
            type: 'post',
            data: {
                cart_uniq_id:cart_uniq_id,
                status: status,
                lat:lat,
                long:long
            },
            success: function (data) {
                console.log(data.trim());
                //$('#modal-body').html(data.trim());
                //$('#modal-delivery').modal('show');

                if(parseInt(data.trim()) > 0){
                    alert("Successfully Updated!");
                    window.location.reload();
                }else{
                    alert("GPS can't locate your location.");
                }
            },
            error: function(e){
                console.log(e.responseText);
            }
        });

        
    }

    document.getElementById("submit-item").onclick = function(){
        navigator.permissions.query({name:'geolocation'})
          .then(function(permissionStatus) {
            console.log('geolocation permission state is ', permissionStatus.state);

            if(permissionStatus.state == "granted"){
                updateOrderToReceived("<?=$return['cart_uniq_id']?>", "Delivered");
            }else{
                if (window.confirm('GPS Location is not allowed on this site, click "ok" to redirect to permissions.')) 
                {  
                    //window.location.href="https://support.google.com/chrome/answer/142065?hl=en&co=GENIE.Platform%3DDesktop&oco=1";
                    window.open("https://support.google.com/chrome/answer/142065?hl=en&co=GENIE.Platform%3DDesktop&oco=1", '_blank');
                };
                getCurrentLocation();
            }

            permissionStatus.onchange = function() {
              console.log('geolocation permission state has changed to ', this.state);
              getCurrentLocation();
            };

          });

    };


$('#sales-receipt').click(function(event) {
    window.open('data/sales_receipt.php?uniqid='+document.getElementById('uniqid').value,'name','width=auto,height=auto');
});
</script>