<?php 
    //require_once('include/session.php'); 
    require_once('database/Database.php');
    require_once('class/System_Setup.php');
?>
<?php

  $system = new System_Setup();
  $system_details = $system->getSystemSetup();
?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title><?=$system_details['system_name']?> Inventory Monitoring System</title>
	
    
        <link rel="stylesheet" href="assets/css/style.css">

    
  </head>

  <body style="background: #262626;">
  <br/>
  <br/>    
    
  

    <div class="container">

  <div id="login-form">

    <h3>User Login</h3>

    <fieldset>

      <form id="form-login">
        <div class="form-group">
          <input type="text" autofocus id="un" placeholder="Username" required autocomplete="off"> 
        </div>
        <div class="form-group">
          <input type="password" id="up" placeholder="Password" required autocomplete="off">
        </div>
        <div class="form-group">
          <div class="row" style="margin-top:20px">
            <input type="submit" name="log" value="Login">
            <a href='register.php'> Click here to Register. </a>
          </div>
        </div>
        
      </form>

    </fieldset>

  </div> <!-- end login-form -->

</div>
    
    
    
<script type="text/javascript" src="assets/js/jquery-1.12.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/regis.js"></script>
    
  </body>
</html>