<link rel="stylesheet" href="assets/qrcodeRead/css/reset.css">
<link rel="stylesheet" href="assets/qrcodeRead/css/styles.css">
<script src="assets/qrcodeRead/js/jquery.min.js"></script>
<script src="assets/qrcodeRead/js/webcam.min.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/grid.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/version.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/detector.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/formatinf.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/errorlevel.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/bitmat.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/datablock.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/bmparser.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/datamask.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/rsdecoder.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/gf256poly.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/gf256.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/decoder.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/qrcode.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/findpat.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/alignpat.js"></script>
<script type="text/javascript" src="assets/qrcodeRead/js/qr/databr.js"></script>
<script src="assets/qrcodeRead/js/effects.js"></script>